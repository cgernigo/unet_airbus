import numpy as np
import torch
from torch import nn
from tqdm import tqdm
from utils.metrics import accuracyIoU

def validation(model: nn.Module, criterion, valid_loader, batch_size, device='cpu'):
    print("Validation on hold-out...")
    model.eval()
    losses = []
    #f2l = []
    iou = []
    tqv = tqdm(total=len(valid_loader) * batch_size // 2)
    tqv.set_description('Validation dataset completion')
    for inputs, targets in valid_loader:
        with torch.no_grad():
            inputs, targets = inputs.to(device), targets.to(device)
            outputs = model(inputs)
            loss = criterion(outputs, targets)
            losses.append(loss.item())
            #f2l.append(f2(targets,outputs))
            iou.append(accuracyIoU(targets,outputs))
            batch_size_loop = inputs.size(0)
            tqv.update(batch_size_loop)

    tqv.close()
    valid_loss = np.mean(losses)
    #valid_f2 = np.mean(f2l)
    valid_iou = np.mean(iou)

    #print('Valid loss: {:.5f}, jaccard: {:.5f}, IoU: {}, f2: {}'.format(valid_loss, valid_jaccard, valid_iou, valid_f2))
    print('Valid loss: {:.5f}, valid IoU: {:.5f}'.format(valid_loss, valid_iou))
    metrics = {'valid_loss': valid_loss, 'valid_IoU': valid_iou}
    return metrics