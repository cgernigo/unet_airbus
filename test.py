import numpy as np
from tqdm import tqdm
import torch
from torch import nn
from utils.metrics import accuracyIoU, F2, f2_kaggle

def test(model: nn.Module, criterion, model_path, test_loader, device='cpu'):
    state = torch.load(model_path)
    #state = {key.replace('module.', ''): value for key, value in state['model'].items()}
    model.load_state_dict(state)
    model.eval()

    f2l = []
    f2k = []
    iou = []
    tqv = tqdm(total=len(test_loader))
    tqv.set_description('Test dataset completion')
    losses = []
    mean_loss = 0
    for inputs, targets in test_loader:
        with torch.no_grad():
            inputs, targets = inputs.to(device), targets.to(device)
            outputs = model(inputs)
            loss = criterion(outputs, targets)
            losses.append(loss.item())
            #f2l.append(F2(targets,outputs).item())
            #f2k.append(f2_kaggle(targets,outputs))
            iou.append(accuracyIoU(targets,outputs))
            batch_size = inputs.size(0)
            tqv.update(batch_size)
    
    tqv.close()
    #test_f2 = np.mean(f2l)
    test_iou = np.mean(iou)
    #test_f2k = np.mean(f2k)
    mean_loss = np.mean(losses)

    #print('Valid loss: {:.5f}, jaccard: {:.5f}, IoU: {}, f2: {}'.format(valid_jaccard, valid_iou, valid_f2))
    print('IoU: {:.5f}'.format(test_iou))
    #print('F2: {:.5f}'.format(test_f2))
    #print('F2k: {:.5f}'.format(test_f2k))
    print('loss: {:.5f}'.format(mean_loss))
    metrics = {'test_loss': mean_loss, 'test_IoU': test_iou}
    return metrics