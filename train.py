import os
import numpy as np
import random
from pathlib import Path
import torch
from torch import nn
from tqdm import tqdm
from utils.utils import write_train_event, write_test_event, get_previous_iou
from utils.metrics import accuracyIoU

# Implementation from  https://github.com/ternaus/robot-surgery-segmentation
def train(lr, model, criterion, train_loader, valid_loader, validation, init_optimizer, batch_size, models_path, logs_path, log_writing_frequency=50, n_epochs=200, fold=1, device='cpu'):
    optimizer = init_optimizer(lr)

    train_log_path = os.path.join(logs_path,'train_{fold}.log'.format(fold=fold))
    test_log_path = os.path.join(logs_path,'test_{fold}.log'.format(fold=fold))
    best_model_path = os.path.join(models_path,'best_model_{fold}.pt'.format(fold=fold))
    train_model_path = os.path.join(models_path,'model_{fold}.pt'.format(fold=fold))

    if Path(train_model_path).exists():
        state = torch.load(train_model_path)
        epoch = state['epoch']
        step = state['step']
        model.load_state_dict(state['model'])
        print('Restored model, epoch {}, step {:,}'.format(epoch, step))
    else:
        epoch = 1
        step = 0


    save = lambda ep: torch.save({
        'model': model.state_dict(),
        'epoch': ep,
        'step': step,
    }, train_model_path)

    log = open(train_log_path, 'at', encoding='utf8')
    log_test = open(test_log_path, 'at', encoding='utf8')
    valid_losses = []
    iou = []
    prec_valid = get_previous_iou(test_log_path)
    for epoch in range(epoch, n_epochs + 1):
        model.train()
        random.seed()
        tq = tqdm(total=len(train_loader) * batch_size)
        tq.set_description('Epoch {}, lr {}'.format(epoch, lr))
        losses = []
        try:
            mean_loss = 0
            for i, (inputs, targets) in enumerate(train_loader):
                inputs, targets = inputs.to(device), targets.to(device)
                outputs = model(inputs)
                loss = criterion(outputs, targets)
                optimizer.zero_grad()
                batch_size_loop = inputs.size(0)
                loss.backward()
                optimizer.step()
                step += 1
                tq.update(batch_size_loop)
                losses.append(loss.data.item())
                iou.append(accuracyIoU(targets,outputs))
                mean_loss = np.mean(losses[-log_writing_frequency:])
                tq.set_postfix(loss='{:.5f}'.format(mean_loss))
                if i and i % log_writing_frequency == 0:
                    write_train_event(log, step, loss=mean_loss)
            train_iou = np.mean(iou)
            metrics = {'train_loss': mean_loss, 'train_IoU': train_iou}
            write_test_event(log_test, epoch, **metrics)
            tq.close()
            print('Train loss: {:.5f}, train IoU: {:.5f}'.format(mean_loss, train_iou))
            save(epoch + 1)
            valid_metrics = validation(model, criterion, valid_loader, batch_size, device=device)
            current_valid = valid_metrics['valid_IoU']
            if prec_valid < current_valid:
                torch.save(model.state_dict(), best_model_path)
            prec_valid = current_valid
            write_test_event(log_test, epoch, **valid_metrics)
            valid_loss = valid_metrics['valid_loss']
            valid_losses.append(valid_loss)
        except KeyboardInterrupt:
            tq.close()
            print('Ctrl+C, saving snapshot')
            save(epoch)
            print('done.')
            return