import os
import numpy as np
from skimage.io import imread

import torch
from torch.utils.data import Dataset
from torchvision.transforms import Compose, ToTensor, Normalize

from utils.masks import masks_as_image, rle_decode

class ShipDataset(Dataset):
    def __init__(self, image_dir, in_df, transform=None):
        grp = list(in_df.groupby('ImageId'))
        self.image_ids = [_id for _id, _ in grp]
        self.image_masks = [m['EncodedPixels'].values for _, m in grp]
        self.transform = transform
        self.image_dir = image_dir
        self.img_transform = Compose([
            ToTensor(),
            Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])

    def __len__(self):
        return len(self.image_ids)

    def __getitem__(self, idx):
        img_file_name = self.image_ids[idx]
        rgb_path = os.path.join(self.image_dir, img_file_name)
        img = imread(rgb_path)
        mask = masks_as_image(self.image_masks[idx])

        if self.transform is not None:
            img, mask = self.transform(img, mask)

        return self.img_transform(img), torch.from_numpy(np.moveaxis(mask, -1, 0)).float()