import numpy as np
from sklearn.metrics import fbeta_score
import torch

def accuracyIoU(y_true, y_pred):
    SMOOTH = 1e-20
    y_pred = torch.ge(y_pred.float(), 0.5).float()
    y_true = y_true.float()
    intersection = (y_pred * y_true).sum()
    union = y_true.sum() + y_pred.sum()

    if y_true.sum() == 0:
        return float(y_pred.sum() == 0)
    else:
        return (intersection / (union - intersection + SMOOTH)).item()

# F2 & Fbeta: https://www.kaggle.com/igormq/f-beta-score-for-pytorch/comments
def F2(y_true, y_pred, threshold=0.5):
    return FBetaScore(y_true, y_pred, 2, threshold)

def FBetaScore(y_true, y_pred, beta, threshold, eps=1e-9):
    beta2 = beta**2

    y_pred = torch.ge(y_pred.float(), threshold).float()
    y_true = y_true.float()

    if y_true.sum().item() == 0.:
        return torch.tensor([y_pred.sum().item() == 0])

    true_positive = (y_pred * y_true).sum(dim=1)
    precision = true_positive.div(y_pred.sum(dim=1).add(eps))
    recall = true_positive.div(y_true.sum(dim=1).add(eps))

    return torch.mean(
        (precision*recall).
        div(precision.mul(beta2) + recall + eps).
        mul(1 + beta2))

# https://www.kaggle.com/markup/f2-metric-optimized
thresholds = [0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]

def iou(y_true, y_pred):
    SMOOTH = 1e-20
    intersection = (y_pred * y_true).sum()
    union = y_true.sum() + y_pred.sum()
    return (intersection / (union - intersection + SMOOTH)).item()

def f2_kaggle(y_true, y_pred):
    if y_true.sum().item() == 0.:
        return float((y_pred > 0.5).sum().item() == 0.)
    
    f2_total = 0
    ious = {}
    for t in thresholds:
        tp,fp,fn = 0,0,0
        for i,mt in enumerate(y_true):
            found_match = False
            for j,mp in enumerate(y_pred):
                key = 100 * i + j
                if key in ious.keys():
                    miou = ious[key]
                else:
                    miou = iou(mt, mp)
                    ious[key] = miou  # save for later
                if miou >= t:
                    found_match = True
            if not found_match:
                fn += 1
                
        for j,mp in enumerate(y_pred):
            found_match = False
            for i, mt in enumerate(y_true):
                miou = ious[100*i+j]
                if miou >= t:
                    found_match = True
                    break
            if found_match:
                tp += 1
            else:
                fp += 1
        #print("t {}, fp {}, tp {}".format(t,fp,tp))

        f2 = (5*tp)/(5*tp + 4*fn + fp)
        #if t == 0.5:
            #print('f2 kaggle',f2)
        #print("f2 {}".format(f2))
        f2_total += f2
    
    return f2_total/len(thresholds)