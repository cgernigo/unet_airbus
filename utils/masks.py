import matplotlib.pyplot as plt
import numpy as np
import cv2
import pandas as pd
from sklearn.model_selection import train_test_split

from utils.analysis import ImageProcessing

# ref: https://www.kaggle.com/paulorzp/run-length-encode-and-decode
def rle_encode(img):
    '''
    img: numpy array, 1 - mask, 0 - background
    Returns run length as string formated
    '''
    pixels = img.flatten()
    pixels = np.concatenate([[0], pixels, [0]])
    runs = np.where(pixels[1:] != pixels[:-1])[0] + 1
    runs[1::2] -= runs[::2]
    return ' '.join(str(x) for x in runs)

def multi_rle_encode(img):
    labels = label(img)
    return [rle_encode(labels==k) for k in np.unique(labels[labels>0])]

def rle_decode(mask_rle, shape=(768, 768)):
    '''
    mask_rle: run-length as string formated (start length)
    shape: (height,width) of array to return
    Returns numpy array, 1 - mask, 0 - background
    '''
    s = mask_rle.split()
    starts, lengths = [np.asarray(x, dtype=int) for x in (s[0:][::2], s[1:][::2])]
    starts -= 1
    ends = starts + lengths
    img = np.zeros(shape[0]*shape[1], dtype=np.uint8)
    for lo, hi in zip(starts, ends):
        img[lo:hi] = 1
    return img.reshape(shape).T  # Needed to align to RLE direction

def mask_overlay(image, mask, color=(0, 1, 0)):
    """
    Helper function to visualize mask on the top of the image
    """
    mask = np.dstack((mask, mask, mask)) * np.array(color)
    weighted_sum = cv2.addWeighted(mask, 0.5, image, 0.5, 0.)
    img = image.copy()
    ind = mask[:, :, 1] > 0
    img[ind] = weighted_sum[ind]
    return img

def masks_as_image(in_mask_list):
    # Take the individual ship masks and create a single mask array for all ships
    all_masks = np.zeros((768, 768), dtype = np.int16)
    #if isinstance(in_mask_list, list):
    for mask in in_mask_list:
        if isinstance(mask, str):
            all_masks += rle_decode(mask)
    return np.expand_dims(all_masks, -1)

def imshow(img, mask, title=None):
    """Imshow for Tensor
    example : imshow(*dataset_test[0]) #To show image with its mask
    """
    img = img.numpy().transpose((1, 2, 0))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    img = std * img + mean
    img = np.clip(img, 0, 1)
    mask = mask.numpy().transpose((1, 2, 0))
    mask = np.clip(mask, 0, 1)
    fig = plt.figure(figsize = (6,6))
    plt.imshow(mask_overlay(img, mask))
    if title is not None:
        plt.title(title)
    plt.pause(0.001)

def ImageSetSplitting(masks_df, nb_image_suppresion, validation_rate = 0.1, testing_rate = 0.1):
    """
    Analyzes the image distribution

    Args:
        masks_df (string): dataframe containing the image masks
        nb_image_suppresion (integer): number of images without ship to remove
        validation_rate (float , default: 0.1): validation image rate
        testing_rate (float , default: 0.1): testing image rate

    Returns: 
        train_df (pandas.core.frame.DataFrame):
        valid_df (pandas.core.frame.DataFrame):
        test_df (pandas.core.frame.DataFrame):
    """
    masks = masks_df.drop(masks_df[masks_df.EncodedPixels.isnull()].sample(nb_image_suppresion, random_state=42).index)
    unique_img_ids = masks.groupby('ImageId').size().reset_index(name='counts')

    train_valid_ids, test_ids = train_test_split(unique_img_ids,
                    test_size = testing_rate,
                    random_state=42
                    )

    train_ids, valid_ids = train_test_split(train_valid_ids,
                    test_size = validation_rate*unique_img_ids.ImageId.nunique()/train_valid_ids.ImageId.nunique(),
                    stratify = train_valid_ids['counts'],
                    random_state=42
                    )

    train_df = pd.merge(masks, train_ids)
    valid_df = pd.merge(masks, valid_ids)
    test_df = pd.merge(masks, test_ids)

    train_df['counts'] = train_df.apply(lambda c_row: c_row['counts'] if
                                        isinstance(c_row['EncodedPixels'], str) else
                                        0, 1)
    valid_df['counts'] = valid_df.apply(lambda c_row: c_row['counts'] if
                                        isinstance(c_row['EncodedPixels'], str) else
                                        0, 1)
    test_df['counts'] = valid_df.apply(lambda c_row: c_row['counts'] if
                                        isinstance(c_row['EncodedPixels'], str) else
                                        0, 1)

    print(r"Total number of images in the dataset {} ({:.2f}% for training, {:.2f}% for validation, {:.2f}% for testing)".format(unique_img_ids.ImageId.nunique(), 100*train_df.ImageId.nunique()/unique_img_ids.ImageId.nunique(), 100*valid_df.ImageId.nunique()/unique_img_ids.ImageId.nunique(), 100*test_df.ImageId.nunique()/unique_img_ids.ImageId.nunique()))

    print('Training image distribution :')
    ImageProcessing(train_df)
    print('Validation image distribution :')
    ImageProcessing(valid_df)
    print('Testing image distribution :')
    ImageProcessing(test_df)

    return train_df, valid_df, test_df