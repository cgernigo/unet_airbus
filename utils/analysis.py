import matplotlib.pyplot as plt
import numpy as np
import statistics as stat
import re

def getParameters(model, type_param="weight"):
    """
    Gets the parameters of a model

    Args:
        model (torch.nn.Module): neural network model
        type_param (string, default: weight): parameter type name (weight, biais, both)
        
    Returns: 
        resList (list): list of model parameters
    """
    #get all parameters into a list of tensors
    paramList = []
    for name, param in model.named_parameters():
        if type_param == "both":
            if param.requires_grad is not None:
                paramList.append(param.data)
        elif type_param == "weight" or type_param == "biais" :
            if param.requires_grad and re.search(type_param,name) is not None:
                paramList.append(param.data)
        else:
            print("It is not a supported type !")
            print("Types accepted : both, weight, biais")

    resList = []

    for i in range(len(paramList)):
        #make a list from a list of unknown size tensors
        resList += paramList[i].view(paramList[i].numel()).tolist()
    
    return resList

""" Régler le problème d'histogramme !? """
def parametersAnalysis(model, type_param="weight"):
    """
    Analyzes the parameters of a model (average, maximum, minimum, histogram)

    Args:
        model (torch.nn.Module): neural network model
        type_param (string, default: weight): parameter type name (weight, biais, both)

    Returns:
        
    """
    flat = getParameters(model, type_param)
    print(flat)
    Max = max(flat)
    Min = min(flat)
    Average = sum(flat)/len(flat)
    #Stdev = stat.stdev(flat)
    #print("Average: {}, max: {}, min: {}, parameters standard deviation: {}".format(Average,Max,Min,Stdev))
    print("Average: {}, max: {}, min: {}".format(Average,Max,Min))
    nbins = 1000
    bin_edges = np.linspace(Min, Max, nbins +1)
    total = np.zeros(nbins, np.uint)
    for i in range(4):
        subtotal, e = np.histogram(flat[(int((int(i>0)+i*len(flat)/4))):(int(((i+1)*len(flat)/4)))], bins=bin_edges)

        total += subtotal.astype(np.uint)
    plt.hist(bin_edges[:-1], bins=bin_edges, weights=total)
    plt.gca().set_xlim(Min,Max)
    plt.xlabel('Parameter values')
    plt.ylabel('Number of parameters')
    plt.pause(0.001)
    plt.show()
    
def ImageProcessing(dataframe):
    """
    Analyzes the image distribution

    Args:
        dataframe (pandas.core.frame.DataFrame): image dataframe
        
    Returns: 
    
    """

    unique_image = dataframe.ImageId.nunique()
    empty_image = dataframe.EncodedPixels.isna().sum()
    ship_image  = unique_image - empty_image

    print("Number of images: {} (images images with ships: {:.2f}%, images without ship: {:.2f}%)".format(unique_image, 100*ship_image/unique_image,100*empty_image/unique_image))