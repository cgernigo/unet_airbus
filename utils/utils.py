import json
import re
from datetime import datetime
import torch
from torch.utils.data import DataLoader
from datasets.airbus import ShipDataset

def write_train_event(log, step: int, **data):
    data['step'] = step
    data['dt'] = datetime.now().isoformat()
    log.write(json.dumps(data, sort_keys=True))
    log.write('\n')
    log.flush()

def write_test_event(log, epoch: int, **data):
    data['epoch'] = epoch
    data['dt'] = datetime.now().isoformat()
    log.write(json.dumps(data, sort_keys=True))
    log.write('\n')
    log.flush()

def get_previous_iou(log_dir):
    logs = open(log_dir,'r')

    lines = logs.read().splitlines()
    logs.close()

    exist = 0

    for line in lines:
        if not line:
            continue

        columns = [col.strip() for col in line.split(',') if col]

        for i in columns:
            if 'valid_IoU' in i:
                pre_iou = re.findall(r"[-+]?\d*\.\d+|\d+", i)
                exist +=1

    if exist != 0:
        return float(pre_iou[0])
    else:
        return 0.0

def make_loader(image_dir, in_df, batch_size, shuffle=False, transform=None):
    return DataLoader(
        dataset=ShipDataset(image_dir, in_df, transform=transform),
        shuffle=shuffle,
        num_workers=0,
        batch_size=batch_size,
        pin_memory=torch.cuda.is_available()
    )

def OnDevice():
    if torch.cuda.is_available():
        return 'cuda'
    else:
        return 'cpu'

def get_jaccard(y_true, y_pred):
    epsilon = 1e-15
    intersection = (y_pred * y_true).sum(dim=-2).sum(dim=-1).sum(dim = -1)
    union = y_true.sum(dim=-2).sum(dim=-1).sum(dim=-1) + y_pred.sum(dim=-2).sum(dim=-1).sum(dim = -1)

    return (intersection / (union - intersection + epsilon)).mean()

def get_fan_in_and_fan_out(tensor):
    dims = tensor.dim()
    num_ifmaps = tensor.size(1)
    num_ofmaps = tensor.size(0)
    receptive_field_size = 1
    if tensor.dim() > 2:
        receptive_field_size = tensor[0][0].numel()
    fan_in = num_ifmaps * receptive_field_size
    fan_out = num_ofmaps * receptive_field_size
    return fan_in, fan_out

def lr_scaling(net, lr, optimizer='Adam'):
    param_list = []
    for m in net.modules():
        if isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
            fan_in, fan_out = get_fan_in_and_fan_out(m.weight)
            lr_scale = math.sqrt(1.5 / float(fan_in + fan_out))
            if optimizer != 'Adam':
                lr_scale *= lr_scale
            param_list.append({'params': m.weight, 'lr': lr / lr_scale})
            param_list.append({'params': m.bias})
        elif isinstance(m, nn.BatchNorm1d) or isinstance(m, nn.BatchNorm2d):
            param_list.append({'params': m.parameters()})
    return param_list