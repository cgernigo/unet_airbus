ENVDIR="$HOME/Environments/dlenv"
WORKDIR="$HOME/Deep_Learning/Airbus_ship_detection/UNet_pytorch"

source ${ENVDIR}/bin/activate
echo "Python virtual environment is activated"
echo "Program start up ..."
${ENVDIR}/bin/python ${WORKDIR}/main.py --mode test --fold 2