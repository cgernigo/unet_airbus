import os
import pandas as pd
import argparse

import torch
from torch.optim import Adam

from models.UNet import UNet
from test import test
from validation import validation
from train import train
from losses.lossbinary import LossBinary
from utils.masks import ImageSetSplitting
from utils.transforms import DualCompose, ImageOnly, HorizontalFlip, VerticalFlip, RandomCrop, RandomBrightness, RandomContrast, CenterCrop
from utils.utils import OnDevice, make_loader, lr_scaling

# old_model_path='/udd/cgernigo/Deep_Learning/Airbus_ship_detection/model_1.pt'

device = OnDevice()

mode_list = {'test',
             'train',
             'both'}

model_list = {'U-Net': UNet}

def main():
        
    parser = argparse.ArgumentParser(description='PyTorch Airbus ship detection training/testing')
    arg = parser.add_argument
    arg('--image_path', default='/srv/tempdd/cgernigo/train_v2', type=str,
        help='Images dataset location')
    arg('--masks_path', default='/srv/tempdd/cgernigo/train_ship_segmentations_v2.csv', type=str,
        help='csv file path')
    arg('--batch_size', default=32, type=int,
        help='Batch size')
    arg('--epochs', default=800, type=int,
        help='Number of epochs to run')
    arg('--fold', default=1, type=int,
        help='model numbering')
    arg('--mode', default='train', type=str, choices=mode_list,
        help='mode')
    arg('--model', default='U-Net', type=str, choices=model_list.keys(),
        help='model')
    arg('--models_path', default='/udd/cgernigo/Deep_Learning/models/', type=str,
        help='models path')
    arg('--logs_path', default='/udd/cgernigo/Deep_Learning/logs/', type=str,
        help='logs path')
    arg('--log_writing_frequency', default=50, type=int,
        help='log writing frequency')
    args = parser.parse_args()

    masks = pd.read_csv(args.masks_path) # csv into panda dataframe
    train_df, valid_df, test_df = ImageSetSplitting(masks, nb_image_suppresion=130000) # splitting the dataset into 3 subsets

    train_transform = DualCompose([
            HorizontalFlip(),
            VerticalFlip(),
            RandomCrop((256,256,3)),
            ImageOnly(RandomBrightness()),
            ImageOnly(RandomContrast()),
    ])

    train_loader = make_loader(args.image_path, train_df, batch_size=args.batch_size, shuffle=True, transform=train_transform)
    valid_loader = make_loader(args.image_path, valid_df, batch_size=args.batch_size // 2, transform=None)
    test_loader = make_loader(args.image_path, test_df, batch_size=1, transform=None)
    
    model_name = model_list[args.model]
    model = model_name()
    model.to(device)

    #lr_scaling(model, lr = 1e-4)

    if args.mode == 'train' or args.mode == 'both':
        train(init_optimizer=lambda lr: Adam(model.parameters(), lr=lr),
              lr = 1e-4,
              n_epochs = args.epochs,
              model=model,
              criterion=LossBinary(jaccard_weight=5),
              train_loader=train_loader,
              valid_loader=valid_loader,
              validation=validation,
              models_path=args.models_path,
              logs_path=args.logs_path,
              log_writing_frequency=args.log_writing_frequency,
              fold=args.fold,
              device=device,
              batch_size = args.batch_size)

    if args.mode == 'test' or args.mode == 'both':
        test(model=model,
             criterion=LossBinary(jaccard_weight=5),
             model_path=os.path.join(args.models_path,'best_model_{fold}.pt'.format(fold=args.fold)),
             test_loader=test_loader,
             device=device)

if __name__ == '__main__':
    main()